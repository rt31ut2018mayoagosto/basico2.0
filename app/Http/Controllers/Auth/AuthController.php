<?php

namespace App\Http\Controllers\Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlestLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
   use AuthenticatesAndRegistersUsers, ThrottlestLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    protected function validator (array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255','email'=> 'requerid|email|max:255|unique:users', 'password' => 'required|confirmed|min:6',
        ]);
    }
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'], 'email'=> $data['email'], 'password' => dycrypt($data['password']),
        ]);
    }

}
